import 'package:flutter/material.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:intl/intl.dart';
import 'package:restaurant_rlutter_ui/generated/i18n.dart';
import 'package:restaurant_rlutter_ui/src/controllers/home_controller.dart';
import 'package:restaurant_rlutter_ui/src/controllers/profile_penyedia_jasa_controller.dart';
import 'package:restaurant_rlutter_ui/src/models/Layanan_model.dart';
import 'package:restaurant_rlutter_ui/src/models/Profile_pj.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

import 'BlockButtonWidget.dart';

class CardProfilePJWidget extends StatelessWidget {
  Datum layanan;
  String heroTag;
  double opacity = 1;
  ProfilePenyediaJasaController con;

  final formatter = new NumberFormat("#,###");

  CardProfilePJWidget({Key key, this.layanan, this.heroTag , this.con}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 340,
      height: 500,
      margin: EdgeInsets.only(left: 20, right: 20, top: 15, bottom: 15),
      decoration: BoxDecoration(
        color: Theme.of(context).primaryColor,
        borderRadius: BorderRadius.all(Radius.circular(10)),
        boxShadow: [
          BoxShadow(color: Theme.of(context).focusColor.withOpacity(0.3), blurRadius: 5, offset: Offset(0, 5)),
        ],
          image: new DecorationImage(image: new NetworkImage('${GlobalConfiguration().getString('base_url_foto')}'+layanan.foto),
              fit: BoxFit.cover)
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Container(
            color: Colors.white,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          "Nama Layanan ",
                          softWrap: false,
                          style: TextStyle(color: Colors.pink,fontSize: 15,),
                        ),
                        Text(
                          layanan.namaLayanan,
                          overflow: TextOverflow.fade,
                          softWrap: false,
                          style: TextStyle(color: Colors.black,fontSize: 15,
                          ),
                        ),
                        SizedBox(height: 5),
                      ],
                    ),
                  Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          "Harga Layanan ",
                          overflow: TextOverflow.fade,
                          softWrap: false,
                          style: TextStyle(color: Colors.pink,fontSize: 15,),
                        ),
                        Text(
                          "Rp. " + formatter.format(int.parse(layanan.harga)),
                          overflow: TextOverflow.fade,
                          softWrap: false,
                          style: TextStyle(color: Colors.black,fontSize: 15,),
                        ),
                        SizedBox(height: 5),
                      ],
                    ),
                ],
              ),
            ),
          ),
          Container(
            color: Colors.white,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
//                  Column(
//                    crossAxisAlignment: CrossAxisAlignment.center,
//                    children: <Widget>[
//                      Text(
//                        "Penyedia Jasa ",
//                        softWrap: false,
//                        style: TextStyle(color: Colors.pink,fontSize: 15,),
//                      ),
//                      Text(
//                        layanan.nama,
//                        overflow: TextOverflow.fade,
//                        softWrap: false,
//                        style: TextStyle(color: Colors.black,fontSize: 15,
//                        ),
//                      ),
//                      SizedBox(height: 5),
//                    ],
//                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  String getStatusButtom(String status) {
    if(status == "0")
      return "Confirm";
    else if (status == "1")
      return "Picked Up";
    else
      return "Picked Up";
  }

  getOpa() {
//    if(restaurant.status == "0")
      return 1.0;
//    else if(restaurant.status == "1")
//      return 1.0;
//    else
//      return 0.0;
  }

  getOpaCan() {
//    if(restaurant.status == "3")
//      return 0.0;
//    else
      return 1.0;
  }

  void sendCancelOrder(BuildContext context) {
//    con.listenForCancelOrder(restaurant.saleId);
//    HomeController h = new HomeController();
//    h.listenForCancelOrder(restaurant.saleId);
//    h.refreshHome();
//    Navigator.of(context).pushNamed('/Pages', arguments: 2);
  }

}

String getStatus(String status) {
  if(status == "0")
    return "Pending";
  else if (status == "1")
    return "Confirm";
  else if (status == "2")
    return "Picked Up";
  else if (status == "3")
    return "Cancel";
  else if (status == "4")
    return "Delivered";
  else
    return "Out for Delivered \n Ready to pickup";
}
