import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:intl/intl.dart';
import 'package:restaurant_rlutter_ui/src/controllers/home_controller.dart';
import 'package:restaurant_rlutter_ui/src/elements/CircularLoadingWidget.dart';
import 'package:restaurant_rlutter_ui/src/models/Layanan_model.dart';
import 'package:restaurant_rlutter_ui/src/models/ScreenArguments.dart';
import 'package:restaurant_rlutter_ui/src/models/user.dart';
import 'package:restaurant_rlutter_ui/src/pages/login.dart';
import 'package:restaurant_rlutter_ui/src/pages/profile_penyedia_jasa.dart';
import 'package:restaurant_rlutter_ui/src/repository/user_repository.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'CardOrderWidget.dart';
import 'package:http/http.dart' as http;

class CardsOrderTodayWidget extends StatefulWidget {
  List<DataLayanan> layanan;

  String heroTag;
  HomeController con;
  User user;

  CardsOrderTodayWidget({Key key, this.layanan, this.heroTag, this.con})
      : super(key: key);

  @override
  _CardsOrderTodayWidgetState createState() => _CardsOrderTodayWidgetState();
}

class _CardsOrderTodayWidgetState extends State<CardsOrderTodayWidget> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return widget.layanan.isEmpty
        ? CircularLoadingWidget(height: 288)
        : Container(
//            height: 1350,
            child: ListView.builder(
              scrollDirection: Axis.vertical,
              itemCount: widget.layanan.length,
              itemBuilder: (context, index) {
                return GestureDetector(
                  onTap: () =>
                      showDialog(
                          context: context,
                          builder: (BuildContext context) {
//                        Alert(context: context, title: "", desc: "Silahkan login untuk order").show();
                            return
                              DynamicDialogOpsi(
                              namaLayanan: widget.con.layanan[index].namaLayanan,
                              hargaLayanan: widget.con.layanan[index].harga,
                              idLayanan: widget.con.layanan[index].idLayanan,
                              idPenyediaJasa:
                              widget.con.layanan[index].idPenyediaJasa,
                            );
                          }),

//                  currentUser.id_customers == null ? Alert(context: context,
//                    type: AlertType.error,
//                    title: "ALERT",
//                    desc: "Silahkan login untuk melakukan order",
//                    buttons: [
//                      DialogButton(
//                        child: Text(
//                          "OK",
//                          style: TextStyle(color: Colors.white, fontSize: 20),
//                        ),
//                        onPressed: () =>  Navigator.push(
//                          context,
//                          MaterialPageRoute(builder: (context) => LoginWidget()),
//                        ),
//                        width: 120,
//                      )
//                    ],
//                  ).show() :
//                      showDialog(
//                      context: context,
//                      builder: (BuildContext context) {
////                        Alert(context: context, title: "", desc: "Silahkan login untuk order").show();
//                        return DynamicDialog(
//                          namaLayanan: widget.con.layanan[index].namaLayanan,
//                          hargaLayanan: widget.con.layanan[index].harga,
//                          idLayanan: widget.con.layanan[index].idLayanan,
//                          idPenyediaJasa:
//                              widget.con.layanan[index].idPenyediaJasa,
//                        );
//                      }),

                  child: CardOrderWidget(
                      layanan: widget.layanan.elementAt(index),
                      con: widget.con,
                      heroTag: widget.heroTag),
                );
              },
            ),
          );
  }
}

class DynamicDialog extends StatefulWidget {
  String namaLayanan, hargaLayanan, idLayanan, idPenyediaJasa;

  DynamicDialog(
      {this.namaLayanan,
      this.hargaLayanan,
      this.idLayanan,
      this.idPenyediaJasa});

  @override
  _DynamicDialogState createState() => _DynamicDialogState();
}

class _DynamicDialogState extends State<DynamicDialog> {
  File images;
  GlobalKey<FormState> addProducFormKey;

  final fdate = new DateFormat('dd-MM-yyyy');
  final ftime = new DateFormat('hh:mm');
  final fdateUp = new DateFormat('yyyy-MM-dd');

  String nama_layanan, harga;
  DateTime dateTime;

  Future getOrder() async {
    User _user = await getCurrentUser();

    var bodya = {
      "id_layanan": widget.idLayanan,
      "id_customer": _user.id_customers,
      "jam_order": ftime.format(dateTime),
      "status": "1",
      "tgl": fdateUp.format(dateTime),
      "id_penyedia_jasa":widget.idPenyediaJasa,
    };

    final String url =
        '${GlobalConfiguration().getString('base_url')}order/add_order';
    final client = new http.Client();
    final response = await client.post(
      url,
      body: bodya,
    );

//    if (response.statusCode == 200) {
//      HomeController f = new HomeController();
//      f.refresh();
//    }
    return response.body;
  }

  Future orderLayanan(BuildContext context) async {
    if (addProducFormKey.currentState.validate()) {
        addProducFormKey.currentState.save();

        getOrder().then((value) {
          print("order value");
          print(json.decode(value)['message']);
//          if(value != null){
////            Scaffold.of(context).showSnackBar(SnackBar(content: Text("dhhhhhh"),));
//          }else{
            Alert(context: context, title: "", desc: json.decode(value)['message']).show();
//          }

        });

    }
  }

  @override
  void initState() {
    super.initState();
    addProducFormKey = new GlobalKey<FormState>();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Center(child: Text("Pesan Layanan")),
      content: Form(
        key: addProducFormKey,
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(18.0),
                child: FlatButton(
                    onPressed: () {
                      DatePicker.showDateTimePicker(context,
                          showTitleActions: true, onChanged: (date) {
                        print('change $date');
                      }, onConfirm: (date) {
                        setState(() {
                          dateTime = date;
                        });
                        print('confirm $date');

                      }, currentTime: DateTime.now(), locale: LocaleType.id);
                    },
                    child: Text('Set Tanggal dan Jam Order'),
                    textColor: Colors.white,
                    color: Colors.green,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5)),
                  ),
              ),
              dateTime == null
                  ? Padding(
                    padding: const EdgeInsets.all(17.0),
                    child: Text(
                        'Tanggal dan Waktu belum dipilih',
                        style: TextStyle(color: Colors.orange),
                      ),
                  )
                  : Padding(
                    padding: const EdgeInsets.all(17.0),
                    child: Text(
                        "Tanggal Order   : " + fdate.format(dateTime) + "\n" + "Jam Order           : " + ftime.format(dateTime),
                        style: TextStyle(color: Colors.blue),
                      ),
                  ),
              Padding(
                padding: const EdgeInsets.all(18.0),
                child: FlatButton(
                  padding: EdgeInsets.all(8),
                  onPressed: () {
                    setState(() {
                      if (dateTime != null) {
                        orderLayanan(context);
//                        Navigator.pop(context);
//                        HomeController f = new HomeController();
//                        f.refresh();
//                        Navigator.of(context).pushNamed('/Pages', arguments: 1);
                      }else{
                        Alert(context: context, title: "", desc: "Tanggal dan Jam Belum Dipilih").show();
                      }
                    });
                  },
                  child: Text("Pesan"),
                  textColor: Colors.white,
                  color: Theme.of(context).accentColor,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5)),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class DynamicDialogOpsi extends StatefulWidget {
  String namaLayanan, hargaLayanan, idLayanan, idPenyediaJasa;

  DynamicDialogOpsi(
      {this.namaLayanan,
        this.hargaLayanan,
        this.idLayanan,
        this.idPenyediaJasa});

  @override
  _DynamicDialogState1 createState() => _DynamicDialogState1();
}

class _DynamicDialogState1 extends State<DynamicDialogOpsi> {


  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Center(child: Text("Opsi")),
      content: Form(
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: FlatButton(
                  padding: EdgeInsets.all(8),
                  onPressed: () {
//                    setState(() {
//                      Navigator.push(
//                        context,
//                          MaterialPageRoute(builder: (context) => ProfilePenyediaJasaWidget()),
//                      );
                      setPJ(widget.idPenyediaJasa);
                      Navigator.of(context).push(
                          MaterialPageRoute(
                              settings: RouteSettings(name: "/ProfilePJ"),
                              builder: (context) => ProfilePenyediaJasaWidget(todo: ScreenArguments(widget.idPenyediaJasa)),
                      ),
                      );
//                      Navigator.pushNamed(context, '/ProfilePJ',);
//                    });
                  },
                  child: Text("Profile"),
                  textColor: Colors.white,
                  color: Theme.of(context).accentColor,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5)),
                ),
              ),
              currentUser.id_customers == null ? Padding(padding: const EdgeInsets.all(10.0),child: Text(""),):
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: FlatButton(
                padding: EdgeInsets.all(8),
                onPressed: () {
      //                    setState(() {
                  Navigator.of(context).pop();
                  showDialog(
                      context: context,
                      builder: (BuildContext context) {

                        return DynamicDialog(
                          namaLayanan: widget.namaLayanan,
                          hargaLayanan: widget.hargaLayanan,
                          idLayanan: widget.idLayanan,
                          idPenyediaJasa: widget.idPenyediaJasa,
                        );

                      });
                },
                child: Text("Pesan"),
                textColor: Colors.white,
                color: Theme.of(context).accentColor,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5)),
              ),
            ),

            ],
          ),
        ),
      ),
    );
  }
}