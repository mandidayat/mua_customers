import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:restaurant_rlutter_ui/src/models/Profile_pj.dart';
import 'package:restaurant_rlutter_ui/src/models/user.dart';

class ProfilePJWidget extends StatelessWidget {
  final DataPj dataPj;
  final List<Data_jadwal> data_jadwal;
  ProfilePJWidget({
    Key key,this.dataPj,this.data_jadwal
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Theme.of(context).accentColor,
        borderRadius: BorderRadius.only(bottomLeft: Radius.circular(30), bottomRight: Radius.circular(30)),
      ),
      child: Column(
        children: <Widget>[
          Container(
            width: double.infinity,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Text(
                  dataPj.nama,
                  style: Theme.of(context).textTheme.headline.merge(TextStyle(color: Theme.of(context).primaryColor)),
                ),
                Text(
                  dataPj.noHp,
                  style: Theme.of(context).textTheme.title.merge(TextStyle(color: Theme.of(context).primaryColor)),
                ),
                Container(
                  width: 300.0,
                  child: Text(
                    dataPj.alamat,
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.caption.merge(TextStyle(color: Theme.of(context).primaryColor)),
                  ),
                ),
                Padding(padding: EdgeInsets.only(top:7.0)),
                Text(
                    'Jadwal Penyedia Jasa', textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.title.merge(TextStyle(color: Theme.of(context).primaryColor))
                ),
                Container(
                margin: EdgeInsets.symmetric(vertical: 20.0),
                height: 40.0,
                child: ListView.builder(
                    // This next line does the trick.
                    scrollDirection: Axis.horizontal,
                    itemCount: data_jadwal.length,
                    itemBuilder: (context, index) {
                      return Padding(
                        padding: const EdgeInsets.only(left: 7,right: 7),
                        child: Column(
                          children: <Widget>[
                            Text(
                              data_jadwal[index].tgl
                                  ,style: TextStyle(color: Theme.of(context).primaryColor)
                            ),
                            Text(
                                data_jadwal[index].status
                                ,style: TextStyle(color: Theme.of(context).primaryColor)
                            ),
                          ],
                        ),
                      );
                    }
//                    children: <Widget>[
//                      Padding(padding:  EdgeInsets.only(left: 7,right: 7)),
//                      Column(
//                        children: <Widget>[
//                          Text(
//                            data_jadwal[0].tgl
//                                ,style: TextStyle(color: Theme.of(context).primaryColor)
//                          ),
//                          Text(
//                              data_jadwal[0].status
//                              ,style: TextStyle(color: Theme.of(context).primaryColor)
//                          ),
//                        ],
//                      ),
//                      Padding(padding:  EdgeInsets.only(left: 7,right: 7)),
//                      Column(
//                        children: <Widget>[
//                          Text(
//                              data_jadwal[1].tgl
//                              ,style: TextStyle(color: Theme.of(context).primaryColor)
//                          ),
//                          Text(
//                              data_jadwal[1].status
//                              ,style: TextStyle(color: Theme.of(context).primaryColor)
//                          ),
//                        ],
//                      ),
//                      Padding(padding:  EdgeInsets.only(left: 7,right: 7)),
//                      Column(
//                        children: <Widget>[
//                          Text(
//                              data_jadwal[2].tgl
//                              ,style: TextStyle(color: Theme.of(context).primaryColor)
//                          ),
//                          Text(
//                              data_jadwal[2].status
//                              ,style: TextStyle(color: Theme.of(context).primaryColor)
//                          ),
//                        ],
//                      ),
//                      Padding(padding:  EdgeInsets.only(left: 7,right: 7)),
//                      Column(
//                        children: <Widget>[
//                          Text(
//                              data_jadwal[3].tgl
//                              ,style: TextStyle(color: Theme.of(context).primaryColor)
//                          ),
//                          Text(
//                              data_jadwal[3].status
//                              ,style: TextStyle(color: Theme.of(context).primaryColor)
//                          ),
//                        ],
//                      ),
//                      Padding(padding:  EdgeInsets.only(left: 7,right: 7)),
//                      Column(
//                        children: <Widget>[
//                          Text(
//                              data_jadwal[4].tgl
//                              ,style: TextStyle(color: Theme.of(context).primaryColor)
//                          ),
//                          Text(
//                              data_jadwal[4].status
//                              ,style: TextStyle(color: Theme.of(context).primaryColor)
//                          ),
//                        ],
//                      ),
//                      Padding(padding:  EdgeInsets.only(left: 7,right: 7)),
//                      Column(
//                        children: <Widget>[
//                          Text(
//                              data_jadwal[5].tgl
//                              ,style: TextStyle(color: Theme.of(context).primaryColor)
//                          ),
//                          Text(
//                              data_jadwal[5].status
//                              ,style: TextStyle(color: Theme.of(context).primaryColor)
//                          ),
//                        ],
//                      ),
//                      Padding(padding:  EdgeInsets.only(left: 7,right: 7)),
//                      Column(
//                        children: <Widget>[
//                          Text(
//                              data_jadwal[6].tgl
//                              ,style: TextStyle(color: Theme.of(context).primaryColor)
//                          ),
//                          Text(
//                              data_jadwal[6].status
//                              ,style: TextStyle(color: Theme.of(context).primaryColor)
//                          ),
//                        ],
//                      ),
//                      Padding(padding:  EdgeInsets.only(left: 7,right: 7)),
//                    ],
                    ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
