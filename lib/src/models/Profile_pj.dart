// To parse this JSON data, do
//
//     final profilePj = profilePjFromJson(jsonString);

import 'dart:convert';

ProfilePj profilePjFromJson(String str) => ProfilePj.fromJson(json.decode(str));

String profilePjToJson(ProfilePj data) => json.encode(data.toJson());

class ProfilePj {
  ProfilePj({
    this.responce,
    this.dataPj,
    this.data,
    this.data_jadwal,
  });

  bool responce;
  List<DataPj> dataPj;
  List<Datum> data;
  List<Data_jadwal> data_jadwal;

  factory ProfilePj.fromJson(Map<String, dynamic> json) => ProfilePj(
    responce: json["responce"],
    dataPj: List<DataPj>.from(json["data_pj"].map((x) => DataPj.fromJson(x))),
    data_jadwal: List<Data_jadwal>.from(json["data_jadwal"].map((x) => Data_jadwal.fromJson(x))),
    data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "responce": responce,
    "data_pj": List<dynamic>.from(dataPj.map((x) => x.toJson())),
    "data_jadwal": List<dynamic>.from(data_jadwal.map((x) => x.toJson())),
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class Datum {
  Datum({
    this.namaLayanan,
    this.idLayanan,
    this.harga,
    this.foto,
    this.idPenyediaJasa,
    this.nama,
  });

  String namaLayanan;
  String idLayanan;
  String harga;
  String foto;
  String idPenyediaJasa;
  String nama;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    namaLayanan: json["nama_layanan"],
    idLayanan: json["id_layanan"],
    harga: json["harga"],
    foto: json["foto"],
    idPenyediaJasa: json["id_penyedia_jasa"],
    nama: json["nama"],
  );

  Map<String, dynamic> toJson() => {
    "nama_layanan": namaLayanan,
    "id_layanan": idLayanan,
    "harga": harga,
    "foto": foto,
    "id_penyedia_jasa": idPenyediaJasa,
    "nama": nama,
  };
}

class DataPj {
  DataPj({
    this.idPenyediaJasa,
    this.nama,
    this.alamat,
    this.noHp,
    this.username,
    this.password,
  });

  String idPenyediaJasa;
  String nama;
  String alamat;
  String noHp;
  String username;
  String password;

  factory DataPj.fromJson(Map<String, dynamic> json) => DataPj(
    idPenyediaJasa: json["id_penyedia_jasa"],
    nama: json["nama"],
    alamat: json["alamat"],
    noHp: json["no_hp"],
    username: json["username"],
    password: json["password"],
  );

  Map<String, dynamic> toJson() => {
    "id_penyedia_jasa": idPenyediaJasa,
    "nama": nama,
    "alamat": alamat,
    "no_hp": noHp,
    "username": username,
    "password": password,
  };
}

class Data_jadwal {
  Data_jadwal({
    this.status,
    this.tgl,
  });

  String status;
  String tgl;

  factory Data_jadwal.fromJson(Map<String, dynamic> json) => Data_jadwal(
    status: json["status"],
    tgl: json["tgl"],
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "tgl": tgl,
  };
}
