import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:restaurant_rlutter_ui/generated/i18n.dart';
import 'package:restaurant_rlutter_ui/src/models/Layanan_model.dart';
import 'package:restaurant_rlutter_ui/src/models/Profile_pj.dart';
import 'package:restaurant_rlutter_ui/src/models/user.dart';
import 'package:restaurant_rlutter_ui/src/repository/order_today_repository.dart';
import 'package:restaurant_rlutter_ui/src/repository/profile_pj_rep.dart';
import 'package:restaurant_rlutter_ui/src/repository/user_repository.dart' as repository;

class ProfilePenyediaJasaController extends ControllerMVC {
  ProfilePj layanan;

  ProfilePenyediaJasaController() {
    listenForOrderToday();
  }

  void listenForOrderToday() async {
    getPJa().then((vale){
      if (vale != null) {
        setState(() {
          layanan = vale;
        });
      }
    });
  }

  void listenForCancelOrder(String id) async {
//    cancelOrder(id).then((vale){
//      if (vale != null) {
//        setState(() {
//          refreshOrdersToday();
//        });
//      }
//    });
  }

  Future<void> refreshOrdersToday() async {
    layanan.data.clear();
    layanan.dataPj.clear();
    listenForOrderToday();

  }

  Future<void> refreshHome() async {
    layanan.data.clear();
    layanan.dataPj.clear();
    listenForOrderToday();
  }
}