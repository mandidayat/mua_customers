import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:restaurant_rlutter_ui/generated/i18n.dart';
import 'package:restaurant_rlutter_ui/src/controllers/home_controller.dart';
import 'package:restaurant_rlutter_ui/src/controllers/profile_controller.dart';
import 'package:restaurant_rlutter_ui/src/controllers/profile_penyedia_jasa_controller.dart';
import 'package:restaurant_rlutter_ui/src/elements/BlockButtonWidget.dart';
import 'package:restaurant_rlutter_ui/src/elements/CardsOrderTodayWidget.dart';
import 'package:restaurant_rlutter_ui/src/elements/CardsProfilePenyediaJasaWidget.dart';
import 'package:restaurant_rlutter_ui/src/elements/CircularLoadingWidget.dart';
import 'package:restaurant_rlutter_ui/src/elements/ProfileAvatarWidget.dart';
import 'package:restaurant_rlutter_ui/src/elements/ProfilePJWidget.dart';
import 'package:restaurant_rlutter_ui/src/models/ScreenArguments.dart';
import 'package:restaurant_rlutter_ui/src/pages/home.dart';
import 'package:restaurant_rlutter_ui/src/pages/login.dart';
import 'package:restaurant_rlutter_ui/src/repository/user_repository.dart';

class ProfilePenyediaJasaWidget extends StatefulWidget {
  final GlobalKey<ScaffoldState> parentScaffoldKey;
  final ScreenArguments todo;

  ProfilePenyediaJasaWidget({Key key, this.parentScaffoldKey,@required this.todo}) : super(key: key);

  @override
  _ProfileWidgetState createState() => _ProfileWidgetState();
}

class _ProfileWidgetState extends StateMVC<ProfilePenyediaJasaWidget> {
  ProfilePenyediaJasaController _con;
  _ProfileWidgetState() : super(ProfilePenyediaJasaController()) {
    _con = controller;
  }

  @override
  Widget build(BuildContext context) {
    final ScreenArguments args = ModalRoute.of(context).settings.arguments;
    final theme = Theme.of(context).copyWith(dividerColor: Colors.transparent);
    return WillPopScope(
        onWillPop: () async => true,
    child: Scaffold(
      appBar: AppBar(
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back, color: Theme.of(context).primaryColor),
          onPressed: () {
              Navigator.of(context).pushReplacementNamed('/Pages', arguments: 1);
            },
        ),
        automaticallyImplyLeading: false,
        backgroundColor: Theme.of(context).accentColor,
        elevation: 0,
        centerTitle: true,
        title: Text(
          'Profile Penyedia Jasa',
          style: Theme.of(context)
              .textTheme
              .title
              .merge(TextStyle(letterSpacing: 1.3, color: Theme.of(context).primaryColor)),
        ),
        actions: <Widget>[
//          new ShoppingCartButtonWidget(
//              iconColor: Theme.of(context).primaryColor, labelColor: Theme.of(context).hintColor),
        ],
      ),
      body: new RefreshIndicator(
        onRefresh: _con.refreshHome,
        child: Container(
          child: Column(children: <Widget>[
            ProfilePJWidget(dataPj: _con.layanan.dataPj[0],data_jadwal:_con.layanan.data_jadwal ,),
            Text(
              "List Layanan",
              style: Theme.of(context).textTheme.display3.merge(TextStyle(color: Theme.of(context).accentColor)),
            ),
            Expanded(
              child: CardsProfilePenyediaJasaWidget(
                  layanan: _con.layanan,
                  con: _con,
                  heroTag: 'home_top_restaurants'),
            ),
          ]),
        ),
      ),
    ),
    );
  }
}
