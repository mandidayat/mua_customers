import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:restaurant_rlutter_ui/generated/i18n.dart';
import 'package:restaurant_rlutter_ui/src/controllers/profile_controller.dart';
import 'package:restaurant_rlutter_ui/src/elements/BlockButtonWidget.dart';
import 'package:restaurant_rlutter_ui/src/elements/CircularLoadingWidget.dart';
import 'package:restaurant_rlutter_ui/src/elements/ProfileAvatarWidget.dart';
import 'package:restaurant_rlutter_ui/src/pages/login.dart';

class ProfileWidget1 extends StatefulWidget {
  final GlobalKey<ScaffoldState> parentScaffoldKey;

  ProfileWidget1({Key key, this.parentScaffoldKey}) : super(key: key);
  @override
  _ProfileWidgetState createState() => _ProfileWidgetState();
}

class _ProfileWidgetState extends StateMVC<ProfileWidget1> {
  ProfileController _con;
  final TextEditingController _textEditingController = new TextEditingController();
  final TextEditingController _textEditingHPController = new TextEditingController();
  final TextEditingController _textEditingAddressController = new TextEditingController();

  _ProfileWidgetState() : super(ProfileController()) {
    _con = controller;
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context).copyWith(dividerColor: Colors.transparent);
//    _textEditingController.text = _con.user.nama; //Set value
//    _textEditingHPController.text = _con.user.noHp; //Set value
//    _textEditingAddressController.text = _con.user.alamat; //Set value
    return Scaffold(
      appBar: AppBar(
//        leading: new IconButton(
//          icon: new Icon(Icons.sort, color: Theme.of(context).hintColor),
//          onPressed: () => widget.parentScaffoldKey.currentState.openDrawer(),
//        ),
        automaticallyImplyLeading: false,
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
        title: Text(
          S.of(context).profile,
          style: Theme.of(context).textTheme.title.merge(TextStyle(letterSpacing: 1.3)),
        ),
        actions: <Widget>[
//          new ShoppingCartButtonWidget(
//              iconColor: Theme.of(context).primaryColor, labelColor: Theme.of(context).hintColor),
        ],
      ),
      key: _con.scaffoldKey,
      body:  Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Center(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        "Anda belum login, Silahkan login untuk melanjutkan",
                        textAlign: TextAlign.center,
                        style: new TextStyle(fontSize:18.0,
                            color: const Color(0xFF000000),
                            fontWeight: FontWeight.bold,
                            fontFamily: "Roboto"),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: BlockButtonWidget(
                      text: Text('Login',
                        style: TextStyle(color: Theme.of(context).primaryColor),
                      ),
                      color: Theme.of(context).accentColor,
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => LoginWidget()),
                        );
                      },
                    ),
                  ),
                ],
            ),
    );
  }
}
